//
//  Dog.swift
//  table
//
//  Created by Aatish Rajkarnikar on 2/11/18.
//  Copyright © 2018 Sanjay Thapa. All rights reserved.
//

import Foundation
import RealmSwift

class Dog: Object {
   @objc dynamic var breed: String = ""
   @objc dynamic var image: String = ""
    
    override static func primaryKey() -> String? {
        return "breed"
    }
}







