//
//  ViewController.swift
//  table
//
//  Created by Sanjay Thapa on 2/10/18.
//  Copyright © 2018 Sanjay Thapa. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import RealmSwift

class ViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var datasource: [Dog] = []
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.dataSource = self
        
        let result =  Array(self.realm.objects(Dog.self))
        self.datasource = result
        
        Alamofire.request("https://dog.ceo/api/breeds/list").responseJSON { (response) in
            if let json = response.result.value as? [String: Any] {
                guard let message = json ["message"] as? [String] else {return}
                for item in message {
                    let newDog = Dog()
                    newDog.breed = item
                    newDog.image = "https://images.fineartamerica.com/images-medium-large-5/portrait-of-a-weimaraner-dog-wolf-shadow-photography.jpg"
                    try! self.realm.write {
                        self.realm.add(newDog, update: true)
                    }
                }
                let result =  Array(self.realm.objects(Dog.self))
                self.datasource = result
                self.tableview.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let dog = datasource[indexPath.row]
        cell?.textLabel?.text = dog.breed
        let imageUrl =  URL(string: dog.image)
        cell?.imageView?.kf.setImage(with: imageUrl)
        return cell!
    }
}

















